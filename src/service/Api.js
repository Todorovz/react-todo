import axios from "axios";

class ApiService {

    baseUrl = 'http://localhost:3001/';

    register(user) {
        return axios.post(this.baseUrl + 'user', user);
    }

    login(user) {
        return axios.post(this.baseUrl + 'user/authorization', user);
    }

    getTodoItems(userId) {
        return axios.get(this.baseUrl + 'todo/get-item?userId=' + userId);
    }
}

export default ApiService;
