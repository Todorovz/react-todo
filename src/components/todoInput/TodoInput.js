import React from 'react';
import './TodoInput.css';

class TodoInput extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="TodoInput">
                <label>{this.props.label}</label>
                <input className="form-control TodoInput"
                       type={this.props.type} onChange={this.props.onChange}
                />
            </div>
        );
    }

}

export default TodoInput;