import React from 'react';
import './TodoButton.css';

class TodoButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="TodoButton">
                <button type={this.props.btnType} className="btn btn-dark">{this.props.btnText}</button>
            </div>
        );
    }

}

export default TodoButton;