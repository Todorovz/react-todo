import React from 'react';
import './TodoListItem.css';

class TodoListItem extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.item.itemStatus === 'Done') {
            return (
                <li className="text-line-through">Item {this.props.item.itemName}</li>
            )
        }
        return (
            <li>Item {this.props.item.itemName}</li>
        );
    }

}

export default TodoListItem;
