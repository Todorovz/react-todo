import React from 'react';
import './TodoList.css';
import TodoListItem from "../todoListItem/TodoListItem";
import ApiService from "../../service/Api";

class TodoList extends React.Component{

    constructor(props) {
        super(props);
        this.apiService = new ApiService();
        this.state = {todoList: []}
    }

    componentDidMount() {
        this.apiService.getTodoItems(localStorage.getItem('userId'))
            .then((response) => {
                this.setState({todoList: response.data});
                console.log(this.state);
            })
            .catch((err) => {
                console.error(err);
            })
    }

    render() {
        let items = [];
        this.state.todoList.forEach((item, index) => {
            items.push(<TodoListItem key={index} item={item} />)
        });
        return (
            <ul>
                {items}
            </ul>
        );
    }

}

export default TodoList;