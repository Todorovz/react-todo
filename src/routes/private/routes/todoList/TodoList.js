import React from 'react'
import TodoList from "../../../../components/todoList/TodoList";

class TodoListPage extends React.Component{

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <TodoList/>
            </div>
        );
    }
}

export default TodoListPage;