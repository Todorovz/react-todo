import React from "react";
import {
    Route,
    Redirect,
} from "react-router-dom";

function PublicRoute({ component: Component, ...rest }) {
    return (
        <Route
            {...rest}
            render={props =>
                !localStorage.getItem('jwtToken') ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/app",
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
}

export default PublicRoute;