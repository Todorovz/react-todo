import React from "react";
import './Login.css';
import { Link } from 'react-router-dom';
import TodoInput from "../../../../components/todoInput/TodoInput";
import TodoButton from "../../../../components/todoButton/TodoButton";
import ApiService from "../../../../service/Api";

class LoginPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {email: '', password: ''};
        this.apiService = new ApiService();

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmitLoginForm = this.handleSubmitLoginForm.bind(this);
    }

    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleSubmitLoginForm(event) {
        this.apiService.login(this.state)
            .then((response) => {
                localStorage.setItem('jwtToken', response.data.jwtToken);
                localStorage.setItem('userId', response.data._id);
                this.props.history.push('/app');
            })
            .catch((err) => {
                console.error(err);
            });
        event.preventDefault();
    }

    render() {
        return (
            <div className="LoginPage">
                <form className="LoginForm" onSubmit={this.handleSubmitLoginForm}>
                    <h3 className="mb-4 text-center">TODO: Login</h3>
                    <TodoInput type="text" label="E-mail" onChange={this.handleChangeEmail}/>
                    <TodoInput type="password" label="Password" onChange={this.handleChangePassword}/>
                    <TodoButton btnText="Login" btnType="submit"/>
                    <div className="RegisterLink">
                        <Link to="/register">You dont have an account? Click here to create new account</Link>
                    </div>
                </form>
            </div>
        );
    }
}

export default LoginPage;

