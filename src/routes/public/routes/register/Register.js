import React from "react";
import './Register.css';
import { Link } from 'react-router-dom';
import TodoInput from "../../../../components/todoInput/TodoInput";
import TodoButton from "../../../../components/todoButton/TodoButton";
import ApiService from "../../../../service/Api";

class RegisterPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {email: '', password: ''};
        this.apiService = new ApiService();

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmitRegisterForm= this.handleSubmitRegisterForm.bind(this);
    }

    handleChangeEmail(event) {
        this.setState({email: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleSubmitRegisterForm(event) {
        this.apiService.register(this.state)
            .then((response) => {
                console.log(response.data);
            })
            .catch((err) => {
                console.error(err);
            });
        event.preventDefault();
    }

    render() {
        return (
            <div className="RegisterPage">
                <div className="BackToLoginBtn"><Link to="/"><button className="btn btn-outline-dark mb-4">Go back</button></Link></div>
                <form className="RegisterForm" onSubmit={this.handleSubmitRegisterForm}>
                    <h3 className="mb-4 text-center">TODO: Register</h3>
                    <TodoInput type="text" label="E-mail" onChange={this.handleChangeEmail}/>
                    <TodoInput type="password" label="Password" onChange={this.handleChangePassword}/>
                    <TodoButton btnText="Register" btnType="submit"/>
                </form>
            </div>
        );
    }
}

export default RegisterPage;