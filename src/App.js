import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import LoginPage from "./routes/public/routes/login/Login";
import RegisterPage from "./routes/public/routes/register/Register";
import PrivateRoute from "./routes/private/PrivateRoute";
import TodoListPage from "./routes/private/routes/todoList/TodoList";
import PublicRoute from "./routes/public/PublicRoute";


class App extends React.Component {

    render() {
        return (
            <Router>
                <div className="App">
                    <PublicRoute path="/" exact component={LoginPage} />
                    <PublicRoute path="/register" component={RegisterPage}  />
                    <PrivateRoute path="/app" component={TodoListPage}/>
                </div>
            </Router>
        );
    }
}

export default App;
